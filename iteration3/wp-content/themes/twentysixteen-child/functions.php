<?php
function my_theme_enqueue_styles() {
    $parent_style = 'parent-style';
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-slick',
        get_stylesheet_directory_uri() . '/slick/slick.css');
    wp_enqueue_style( 'child-font-awesome',
        get_stylesheet_directory_uri() . '/fonts/font-awesome.css');
    wp_enqueue_style( 'child-bootstrap',
        get_stylesheet_directory_uri() . '/css/bootstrap.css');
     wp_enqueue_style( 'child-hamburger',
        get_stylesheet_directory_uri() . '/css/hamburger.css');
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/css/custom-style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
    wp_enqueue_script( 'slick_js', get_stylesheet_directory_uri() . '/slick/slick.js',
     array( 'jquery' ), '1.0', true );
    wp_enqueue_script( 'bootstrap_js', get_stylesheet_directory_uri() . '/js/bootstrap.js',
     array( 'jquery' ), '1.0', true );
    wp_enqueue_script( 'custom_js', get_stylesheet_directory_uri() . '/js/custom.js',
     array( 'jquery' ), '1.0', true );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

add_image_size( 'admin-list-thumb', 80, 80, false );

function wpcs_add_thumbnail_columns( $columns ) {
    $columns = array(
        'cb' => '<input type="checkbox" />',
        'featured_thumb' => 'Thumbnail',
        'title' => 'Title',
        'author' => 'Author',
        'categories' => 'Categories',
        'tags' => 'Tags',
        'comments' => '<span class="vers"><div title="Comments" class="comment-grey-bubble"></div></span>',
        'date' => 'Date'
    );
    return $columns;
}

function wpcs_add_thumbnail_columns_data( $column, $post_id ) {
    switch ( $column ) {
    case 'featured_thumb':
        echo '<a href="' . get_edit_post_link() . '">';
        echo the_post_thumbnail( 'admin-list-thumb' );
        echo '</a>';
        break;
    }
}

if ( function_exists( 'add_theme_support' ) ) {
    add_filter( 'manage_posts_columns' , 'wpcs_add_thumbnail_columns' );
    add_action( 'manage_posts_custom_column' , 'wpcs_add_thumbnail_columns_data', 10, 2 );
    add_filter( 'manage_pages_columns' , 'wpcs_add_thumbnail_columns' );
    add_action( 'manage_pages_custom_column' , 'wpcs_add_thumbnail_columns_data', 10, 2 );
}

?>