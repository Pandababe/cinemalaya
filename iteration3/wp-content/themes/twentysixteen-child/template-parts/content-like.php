<div class="col-md-4">
  <iframe src="http://a.abc.com/ad/abcdigital/oscars/ABC_Oscars_300x250_DESKTOP_PROG_DCM.html" height="250" width="300" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true"></iframe>
  <h3 style="margin: 0 0 10px 5px;">You may also like...</h3>
  <div class="row">
  <?php
  $args_1 = array(
    'category_name' => 'article',
    'tag' => 'side-left',
    'order' => 'ASC'
  );
  $custom_query_1 = new WP_query($args_1);
  while ( $custom_query_1->have_posts() ): $custom_query_1->the_post(); ?>
  <div class="home_item col-md-12 col-xs-6"><a href="<?php the_permalink(); ?>">	
    
      <div class="col-md-4"> 
        <p class="like-image"><?php echo get_the_post_thumbnail(); ?></p>
      </div>
      <div class="col-md-8">
        <p class="like_title"><?php the_title(); ?></p>
        <p><?php $content = get_field('article_short_description'); echo mb_strimwidth($content, 0, 25, '...');?></p>
        <p class="author"><?php echo get_field('article_author'); ?></p>
      </div>
    </div>
    </a>
  <?php
  endwhile;
  ?>
  </div>
</div>