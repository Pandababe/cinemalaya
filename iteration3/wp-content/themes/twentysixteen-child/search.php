<?php
/**
 * The template for displaying search results pages
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
<div class="site-inner">
	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<header class="page-header">
				<h1><?php printf( __( 'Search Results for: %s', 'twentysixteen' ), '<span>' . esc_html( get_search_query() ) . '</span>' ); ?></h1>
				<div class="header-divider"></div>
			</header><!-- .page-header -->
			<div class="row">
			<div class="col-md-8 left_bd">
		<?php if ( have_posts() ) : ?>



			<?php
			// Start the loop.
			while ( have_posts() ) : the_post();

				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'search' );

			// End the loop.
			endwhile;



		// If no content, include the "No posts found" template.
		else :
			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

			</div>
              <?php get_template_part( 'template-parts/content', 'like' ); ?>
		</div>
		</main><!-- .site-main -->
	</section><!-- .content-area -->
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
