<?php
/**
 * The template part for displaying content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php if ( is_sticky() && is_home() && ! is_paged() ) : ?>
			<span class="sticky-post"><?php _e( 'Featured', 'twentysixteen' ); ?></span>
		<?php endif; ?>
	</header><!-- .entry-header -->
	<div class="row cat-item-list">
	<div class="col-md-5 img">
	<?php twentysixteen_post_thumbnail(); ?>
	</div>
	<div class="col-md-7 desc">
		<a href="<?php echo the_permalink(); ?>">
			<h2 class="entry-title"><?php the_title(); ?></h2>
			<p><?php echo get_field('article_short_description'); ?></p>
			<p class="author"><?php echo get_field('article_author'); ?></p>
		</a>
	</div>
	</div>
</article><!-- #post-## -->
