<?php get_header(); ?>

<div class="home_page">

	<div class="container">
		<div class="row featured">
			<div class="col-md-6">
				<div class="grid slider">

							<?php
			$args_2 = array(
				'category_name' => 'article',
				'tag' => 'feature-articles',
				'order' => 'ASC'
				);
			$custom_query_2 = new WP_query($args_2);
			while ( $custom_query_2->have_posts() ): $custom_query_2->the_post();
			$src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large', false );
			?>

				<figure class="effect-apollo">
					
					<img src="<?php echo $src[0]; ?>" alt="">
					<figcaption>
						<h2><?php the_title(); ?></h2>
						<p><i class="fa fa-file-text" aria-hidden="true"></i></p>
						<a href="<?php the_permalink(); ?>">View more</a>
					</figcaption>			
				</figure>

			<?php
				endwhile;
			?>

				</div>
			</div>
			<div class="col-md-6 sub">

			<?php
			$args_1 = array(
				'category_name' => 'article',
				'tag' => 'side-left',
				'order' => 'ASC'
				);
			$custom_query_1 = new WP_query($args_1);
			while ( $custom_query_1->have_posts() ): $custom_query_1->the_post();
			$src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false );
			?>

				<div class="col-md-6 col-xs-6">
					<div class="grid">
						<figure class="effect-apollo">
							<img src="<?php echo $src[0]; ?>" alt="">
							<figcaption>
								<h2><?php the_title(); ?></h2>
								<p><i class="fa fa-file-text" aria-hidden="true"></i></p>
								<a href="<?php the_permalink(); ?>">View more</a>
							</figcaption>			
						</figure>
					</div>
				</div>


			<?php
				endwhile;
			?>

			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
			<?php
			$args_1 = array(
				'category_name' => 'article',
				'tag' => 'normal-articles',
				'order' => 'ASC',
				'posts_per_page'=> 5
				);
			$custom_query_1 = new WP_query($args_1);
			while ( $custom_query_1->have_posts() ): $custom_query_1->the_post();
			$src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false );
			?>
				<div class="article-items">
					<a href="<?php the_permalink(); ?>">
						<div class="date">
							<span class="day"><?php echo get_the_date('d'); ?></span><span class="month"><?php echo get_the_date('M'); ?></span><span class="year"><?php echo get_the_date('Y'); ?></span>
						</div>
						<div class="image">
							<p class="browse"><i class="fa fa-file-text" aria-hidden="true"></i></p>
							<img src="<?php echo $src[0]; ?>" alt="">
						</div>
						<div class="detail">
							<h2><?php the_title(); ?></h2>
							<p><?php $content = get_the_content(); echo mb_strimwidth($content, 0, 70, '...');?></p>
						</div>
					</a>
				</div>
			<?php
				endwhile;
			?>
			</div>
			<div class="col-md-4">
				<div class="sponsor">
					<iframe src="http://a.abc.com/ad/abcdigital/oscars/ABC_Oscars_300x250_DESKTOP_PROG_DCM.html" height="250" width="300" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true"></iframe>
				</div>
			</div>
		</div>
	</div>

	<div class="editor_banner" style="background-image: url(<?php echo site_url(). '/wp-content/uploads/2018/03/Picture10.png';?>)">
		<div class="overlay"></div>
		<h1>Editor's Pick</h1>
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<?php
				$args_4 = array(
					'category_name' => 'article',
					'tag' => 'editors-pick',
					'order' => 'ASC'
					);
				$custom_query_4 = new WP_query($args_4);
				while ( $custom_query_4->have_posts() ): $custom_query_4->the_post(); ?>

				<div class="col-sm-4 editor_pick_cont">
					<a href="<?php the_permalink(); ?>">
						<div class="icon"><?php echo get_the_post_thumbnail(); ?></div>
						<p><?php the_title() ?></p>
					</a>
				</div>

				<?php
					endwhile;
				?>
				

			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-8">
			<?php
			$args_1 = array(
				'category_name' => 'article',
				'tag' => 'side-right',
				'order' => 'ASC',
				'posts_per_page'=> 5
				);
			$custom_query_1 = new WP_query($args_1);
			while ( $custom_query_1->have_posts() ): $custom_query_1->the_post();
			$src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false );
			?>
				<div class="article-items">
					<a href="<?php the_permalink(); ?>">
						<div class="date">
							<span class="day"><?php echo get_the_date('d'); ?></span><span class="month"><?php echo get_the_date('M'); ?></span><span class="year"><?php echo get_the_date('Y'); ?></span>
						</div>
						<div class="image">
							<p class="browse"><i class="fa fa-file-text" aria-hidden="true"></i></p>
							<img src="<?php echo $src[0]; ?>" alt="">
						</div>
						<div class="detail">
							<h2><?php the_title(); ?></h2>
							<p><?php $content = get_the_content(); echo mb_strimwidth($content, 0, 70, '...');?></p>
						</div>
					</a>
				</div>
			<?php
				endwhile;
			?>
			</div>
			<div class="col-md-4">
				<div class="sponsor">
					<iframe src="http://a.abc.com/ad/abcdigital/oscars/ABC_Oscars_300x250_DESKTOP_PROG_DCM.html" height="250" width="300" marginwidth="0" marginheight="0" frameborder="0" scrolling="no" allowtransparency="true"></iframe>
				</div>
			</div>
		</div>
	</div>

</div>
<?php get_footer(); ?> 