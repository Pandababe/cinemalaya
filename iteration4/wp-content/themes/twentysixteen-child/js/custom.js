jQuery(document).ready(function(){
 jQuery('.menu-toggle').click(function(){
 	jQuery(this).toggleClass('is-active');
 });
 jQuery('.slider').slick({
 	autoplay: true,
  autoplaySpeed: 2000,
  dots: true,
  prevArrow: false,
  nextArrow: false
 });

var lastScrollTop = 0;
jQuery(window).scroll(function(event){
   var st = jQuery(this).scrollTop(),
       ss = jQuery('.body_content').offset().top - 195;
   if (st > lastScrollTop){
        jQuery('.heading-area').removeClass('active');
        jQuery('.heading-area').css('animation', 'slide-up 0.5s');
   } 
    else {
    	jQuery('.heading-area').addClass('active');
      jQuery('.heading-area').css('animation', 'slide-down 0.5s');
   }

    lastScrollTop = st;
   jQuery(".editor_banner").css("background-position","100% " + (jQuery(this).scrollTop() / 15) + "px");
});



});