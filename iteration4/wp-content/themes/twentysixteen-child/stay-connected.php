<?php
	/*
	Template name: Stay Connected
	*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<?php wp_head();?>
</head>
<body>

	<div id="stay-connected">
		<p class="backbutton"><a onclick="goBack()"><i class="fa fa-arrow-left" aria-hidden="true"></i></a></p>
		<div>
			<img src="http://throughtheeyes.info/iteration4/wp-content/uploads/2018/03/Picture8.png" alt="">
			<div class="row">
				<div class="col-sm-6 no-padding"><img src="http://throughtheeyes.info/iteration4/wp-content/uploads/2018/03/Picture14.png" alt=""></div>
				<div class="col-sm-6 no-padding pull-up"><img src="http://throughtheeyes.info/iteration4/wp-content/uploads/2018/03/Picture8-1-e1527034594359.png" alt=""></div>
			</div>
		</div>
	<div class="bg-orange">
		<div class="container">
			<h2 class="head">Stay Tuned for the <br>next edition of<br> our magazine!</h2>
			<p class="desc">We will be featuring featuring articles <br>about the top-ranking problems in <br> our country reflected through <br> independent cinema</p>
			<div class="col-md-4">
			<?php echo do_shortcode('[email-subscribers namefield=”YES” group=”Public”]');?>	
			</div>
		</div>
	</div>
	
</div>
	<?php wp_footer(); ?>
	<script>
		function goBack() {
		    window.history.back();
		}
	</script>
</body>
</html>